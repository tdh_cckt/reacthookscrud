const { useState, useEffect } = require("react")

const useForm = (callback, validate) => {
    const [values, setValues] = useState({});
    const [errors, setErros] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    useEffect(() => {
        if (Object.keys(errors).length === 0 && isSubmitting) {
            callback();
        }
    }, [errors]);

    const handleSubmit = (event) => {
        if (event) event.preventDefault();
        setIsSubmitting(true);
        setErros(validate(values));
    }

    const handleChange = (event) => {
        event.persist();
        setValues(values => (
            { ...values, [event.target.name]: event.target.value }
        ))
        console.log(values);
    };

    return {
        handleChange,
        handleSubmit,
        values,
        errors
    }
}
export default useForm;