import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import React, { useState, useEffect, Fragment } from 'react';

const AddUserFormModal = (props) => {
    const initialFormState = {
        id: null,
        first_name: '',
        last_name: '',
        email: ''
    }

    const [user, setUser] = useState(initialFormState)
    const handleInputChange = (event) => {
        const { name, value } = event.target

        setUser({ ...user, [name]: value })
    }
    return (
        <Modal show={true} onHide={() => props.handleClose()}>
            <Modal.Header closeButton>
                <Modal.Title>New User</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={(event) => {
                    event.preventDefault()

                    props.addUser(user);
                    props.handleClose();
                }}>
                    <Form.Group>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                        type="text"
                        name="first_name"
                        value={user.name}
                        onChange={handleInputChange}
                    />
                    </Form.Group>
                    <Form.Group>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        name="last_name"
                        value={user.last_name}
                        onChange={handleInputChange}
                    />
                    </Form.Group>
                    <Form.Group>
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        type="text"
                        name="email"
                        value={user.email}
                        onChange={handleInputChange}
                    />
                    </Form.Group>
                    <Button type="submit" variant="primary" >New user</Button>
                    <Button variant="secondary" onClick={()=>{props.handleClose()}}>Close</Button>
                </Form>
            </Modal.Body>
        </Modal>
    )
}; export default AddUserFormModal