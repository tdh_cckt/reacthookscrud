import React, { useState, useEffect } from 'react'
import { Modal, Form, Button } from 'react-bootstrap'

const EditUserFormModal = (props) => {
    const [user, setUser] = useState(props.userToEdit)

    useEffect(
        () => {
            setUser(props.userToEdit)
        },
        [props]
    )

    const handleInputChange = (event) => {
        const { name, value } = event.target

        setUser({ ...user, [name]: value })
    }

    return (
        <Modal show={true} onHide={() => props.handleClose()}>
            <Modal.Header closeButton>
                <Modal.Title>Edit User</Modal.Title>
            </Modal.Header>
            {/* <Modal.Body>
                <Form
                    onSubmit={(event) => {
                        event.preventDefault()

                        props.updateUser(user.id, user)
                    }}
                >
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                        type="text"
                        name="first_name"
                        value={user.first_name}
                        onChange={handleInputChange}
                    />
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        name="last_name"
                        value={user.last_name}
                        onChange={handleInputChange}
                    />
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        type="text"
                        name="email"
                        value={user.email}
                        onChange={handleInputChange}
                    />
                    <Modal.Footer>

                    
                    <Button type="submit" variant="primary">Update user</Button>
                    <Button 
                        variant="secondary"
                        onClick={() => props.setEditing(false)}
                        className="button muted-button"
                    >
                        Cancel
                    </Button>
                    </Modal.Footer>
                </Form>
            </Modal.Body> */}
        </Modal>

    )
}

export default EditUserFormModal