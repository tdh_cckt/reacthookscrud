import React, { useState, forwardRef, useImperativeHandle } from "react";

const InputField = forwardRef((props, ref) => {
  //
  const [value, setValue] = useState("");
  const [error, setError] = useState("");

  const handleChange = (e) => {
    setValue(e.target.value);
    setError("");
    props.onChange(e.target.name, e.target.value);
  };

  const validate = () => {
    //return true if is valid else return false
    if (props.validation) {
      const rules = props.validation.split("|");

      for (let i = 0; i < rules.length; i++) {
        const current = rules[i];

        if (current === "required") {
          if (!value) {
            setError("This field is required!");
            return false;
          }
        }

        const pair = current.split(":");
        switch (pair[0]) {
          case "min":
            if (value.length < pair[1]) {
              setError(`This field must be at least ${pair[1]}!`);
              return false;
            }
            break;
          case "max":
            if (value.length > pair[1]) {
              setError(
                `This field must be no longer ${pair[1]} character long!`
              );
              return false;
            }
            break;

          default:
            break;
        }
      }
    }
    return true;
  };
  useImperativeHandle(ref, () => {
    return { validate: () => validate() };
  });
  return (
    <div>
      {props.label && <label>{props.label}</label>}
      <input
        placeholder={props.placeholder}
        name={props.name}
        onChange={(event) => handleChange(event)}
        type={props.type}
        value={props.value ? props.value : value}
        autoComplete={props.autoComplete}
      />
      <div>
      {props.small && <small>{props.small}</small>}
      </div>
      {error && <p className="error">{error}</p>}
    </div>
  );
});
InputField.defaultProps = {
  placeholder: "",
  name: "",
  onChange: "",
  type: "text",
  value: "",
  autoComplete: "off",
  validation: "",
  helpText: "Username"
};
export default InputField;
