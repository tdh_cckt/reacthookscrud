import React, { useState, useEffect, Fragment } from 'react';
export default function validate(values) {
    let errors = {};
    if (!values.email) {
        errors.email = "email required"
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
        errors.email = 'Email address is invalid';
      }
    return errors;
}