import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import AddUserFormModal from './modals/AddUserFormModal';
import EditUserFormModal from './modals/EditUserFormModal';
import UserTable from './tables/UserTable';
import LoginForm from './forms/LoginForm';

const App = () => {
    const initialFormState = {
        id: null,
        first_name: '',
        last_name: '',
        email: ''
    }
    const [users, setUsers] = useState([]);
    const [isEditing, setIsEditing] = useState(false)
    const [userToEdit, setUserToEdit] = useState(initialFormState)

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const editUser = (user) => {
        setIsEditing(true)

        setUserToEdit(user)
        console.log(user)
    }

    const updateUser = (id, updatedUser) => {
        setIsEditing(false)

        setUsers(users.map((user) => (user.id === id ? updatedUser : user)))
    }

    const addUser = (user) => {
        user.id = users.length + 1
        setUsers([...users, user])
    }

    const deleteUser = (id) => {
        fetch("https://reqres.in/api/users/" + id, { method: 'DELETE' }).then((response) => {
            console.log("delete completed code: " + response.status)
        });
        const newUsers = users.filter((user) => user.id !== id);
        console.log(newUsers);
        setUsers(newUsers);
    }

    useEffect(() => {
        fetch('https://reqres.in/api/users?page=2')
            .then(response => response.json())
            .then(jsonData => setUsers(jsonData.data))
    }, []);
    
    return (
        <div className="container">
            <div className="flex-row">
                <div className="flex-large">
                    <div style={{ display: 'flex' }}>
                        <h2>View users</h2>
                        <Button variant="primary"
                            style={{ position: 'absolute', float: 'right', right: '130px' }}
                            onClick={handleShow}
                        >
                            Add New
                        </Button>
                        {
                            console.log(show),
                            show ? <AddUserFormModal
                                addUser={addUser}
                                handleClose={handleClose} /> : (<div></div>)
                        }
                    </div>
                    <UserTable users={users} deleteUser={deleteUser} editUser={editUser} />
                </div>
            </div>
            <div className="flex-large">
                {(isEditing) ? (
                    <div>
                        <h2>Edit user</h2>
                        <EditUserFormModal
                            editing={isEditing}
                            setEditing={setIsEditing}
                            userToEdit={userToEdit}
                            updateUser={updateUser}
                            handleClose={handleClose} />
                    </div>


                ) : (<div></div>
                    )}
            </div>
            <LoginForm/>
        </div>
    )
}

export default App