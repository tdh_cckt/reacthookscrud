import React from 'react'
import Table from 'react-bootstrap/Table';
const UserTable = (props) => (
    <Table striped bordered hover>
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {props.users.length > 0 ? (
                props.users.map((user) => (
                    <tr key={user.id}>
                        <td>{user.first_name}</td>
                        <td>{user.last_name}</td>
                        <td>{user.email}</td>
                        <td>
                            <button className="button muted-button" onClick={() => {
                                props.editUser(user)
                            }}>Edit</button>
                            <button className="button muted-button" onClick={() => {
                                props.deleteUser(user.id)
                            }}>Delete</button>
                        </td>
                    </tr>
                ))
            ) : (
                    <tr>
                        <td colSpan={3}>No users</td>
                    </tr>
                )}
        </tbody>
    </Table>
)

export default UserTable