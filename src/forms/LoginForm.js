import React, { createRef, useRef, useState } from 'react';
import InputField from '../validatior/InputField'



function LoginForm() {
    const inputRefs = useRef([createRef(), createRef()]);
    const [data, setData] = useState({});
    const handleChange = (name, value) => {
        setData((prev) => ({ ...prev, [name]: value }));
    };

    const submitForm = (e) => {
        e.preventDefault();

        let isValid = true;

        for (let i = 0; i < inputRefs.current.length; i++) {
            const valid = inputRefs.current[i].current.validate();
            if (!valid) {
                isValid = false;
            }
        }
        if (!isValid) {
            return;
        }
    };
    return (
        <div className="App">
            <form onSubmit={submitForm} className="form">
                <InputField
                    ref={inputRefs.current[0]}
                    placeholder="Enter..."
                    name="username"
                    label="UserName"
                    small="We'll never share your email with anyone else."
                    onChange={handleChange}
                    validation={"required|min:6|max:12"}
                />
                <InputField
                    ref={inputRefs.current[1]}
                    name="password"
                    label="Mật khẩu*:"
                    type="password"
                    onChange={handleChange}
                    validation="required|min:6"
                />
                <button type="submit">Login</button>
            </form>
        </div>
    );
}

export default LoginForm;
