import React, { useState } from 'react'

const AddUserForm = (props) => {
    const initialFormState = {
        id: null,
        first_name: '',
        last_name: '',
        email: ''
    }

    const [user, setUser] = useState(initialFormState)
    const handleInputChange = (event) => {
        const { name, value } = event.target

        setUser({ ...user, [name]: value })
    }
    return (
        <form onSubmit={(event) => {
            event.preventDefault()

            props.addUser(user)
            setUser(initialFormState)
        }}>
            <label>First Name</label>
            <input
                type="text"
                name="first_name"
                value={user.name}
                onChange={handleInputChange}
            />
            <label>Last Name</label>
            <input
                type="text"
                name="last_name"
                value={user.last_name}
                onChange={handleInputChange}
            />
            <label>Email</label>
            <input
                type="text"
                name="email"
                value={user.email}
                onChange={handleInputChange}
            />
            <button>Add new user</button>
        </form>
    )
}

export default AddUserForm